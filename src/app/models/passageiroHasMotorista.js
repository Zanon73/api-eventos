import Sequelize, { Model } from 'sequelize';

class PassageiroHasMotorista extends Model {
  static init(sequelize) {
    super.init({
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      id_carona: Sequelize.INTEGER,
      id_passageiro: Sequelize.INTEGER,
    },
      {
        sequelize,
        freezeTableName: 'passageiro_has_motorista',
        tableName: 'passageiro_has_motorista'
      },
    );
    return this;
  } 
  
  // static associate(models) {
  //   this.belongsTo(models.motoristaHasEvento, {foreignKey: 'id', as: 'id_carona'});
  //   this.belongsTo(models.usuario, {foreignKey: 'id', as: 'id_passageiro'});
  // }
}

export default PassageiroHasMotorista;