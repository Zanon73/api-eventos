import * as Yup from 'yup';
import { Op } from 'sequelize';
import categoria from '../models/categoria';

class CategoriaController {

  async getAllCategories(req, res) {
    const categories = await categoria.findAll();

    if (!categories) {
      return res.status(400).json({ error: 'Nenhuma categoria encontrada' });
    }

    return res.json(categories);
  }
  
  async getCategory(req, res) {
    const schema = Yup.object().shape({
      id: Yup.number().required()
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Informe os dados corretamente' });
    }
    
    
    const category = await categoria.findOne({
      where: {
        id: req.body.id
      },
      attributes: ['id', 'nome']
    });

    if (!category) {
      return res.status(400).json({ error: 'Categoria não existente' });
    }

    return res.json(category);
  }
  
}

export default new CategoriaController();